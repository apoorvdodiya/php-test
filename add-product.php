<?php
require_once 'config.php';
$sku = null;
$name = null;
$price = null;
$productType = null;
$size = null;
$weight = null;
$height = null;
$width = null;
$length = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $sku = trim($_POST["sku"]);
  $name = trim($_POST["name"]);
  $price = trim($_POST["price"]);
  $productType = trim($_POST["productType"]);
  $size = trim($_POST["size"]);
  $weight = trim($_POST["weight"]);
  $height = trim($_POST["height"]);
  $width = trim($_POST["width"]);
  $length = trim($_POST["length"]);
}

if (
  // !empty($length) &&
  !empty($sku) &&
  !empty($name) &&
  !empty($price) &&
  !empty($productType)
  // !empty($size) &&
  // !empty($weight) &&
  // !empty($height) &&
  // !empty($width)
) {
  try {
    $sql = 'INSERT INTO product (
          sku,
          name,
          price,
          productType,
          size,
          weight,
          height,
          width,
          length
        ) VALUES (
          "' . ($sku ? $sku : "") . '",
          "' . ($name ? $name : "") . '",
          ' . ($price ? $price : 0) . ',
          "' . ($productType ? $productType : "") . '",
          ' . ($size ? $size : 0) . ',
          ' . ($weight ? $weight : 0) . ',
          ' . ($height ? $height : 0) . ',
          ' . ($width ? $width : 0) . ',
          ' . ($length ? $length : 0) . '
        );';
    $stmt = mysqli_prepare($conn, $sql);
    $res = mysqli_stmt_execute($stmt);
    // header('Location: http://localhost:8080/product-test');
    header('Refresh: 0; url=/product-test');
  }catch (Exception $ex) {
    echo 'SKU already exists';
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="common.css">

  <title>Add Product</title>
</head>

<body onload="onSelectProductType('<?php echo $productType ?>')">
  <div class="p-4">
    <div class="d-flex justify-content-between border-bottom pb-1">
      <h4 class="pb-0">
        Product Add
      </h4>
      <div>
        <button type="submit" class="btn btn-primary btn-sm me-2" onClick="submitForm()">SAVE</button>
        <a id="delete-product-btn" class="btn btn-primary btn-sm" href="index.php">CANCEL</a>
      </div>
    </div>
    <div>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="product_form">
        <div class="row mt-2">
          <div class="col-3 mb-2">
            SKU
          </div>
          <div class="col-9 mb-2">
            <input type="text" name="sku" id="sku" value="<?php echo $sku ?>">
          </div>
          <div class="col-3 mb-2">
            Name
          </div>
          <div class="col-9 mb-2">
            <input type="text" name="name" id="name" value="<?php echo $name ?>">
          </div>
          <div class="col-3 mb-2">
            Price
          </div>
          <div class="col-9 mb-2">
            <input type="number" name="price" id="price" value="<?php echo $price ?>">
          </div>
          <div class="d-flex my-2">
            <div class="me-2">Type Switcher</div>
            <select onchange="onSelectProductType()" name="productType" id="productType">
              <option id="DVD" value="dvd">DVD</option>
              <option id="Book" value="book">Book</option>
              <option id="Furniture" value="furniture">Furniture</option>
            </select>
          </div>
        </div>
        <div class="row" id="dvdForm">
          <div class="col-3 my-1">
            Size
          </div>
          <div class="col-9 my-1">
            <input type="number" name="size" id="size" value="<?php echo $size ?>">
          </div>
          <div>
            Please Provide size/capacity for the DVD in MB
          </div>
        </div>
        <div class="row" id="bookForm">
          <div class="col-3 my-1">
            Weight
          </div>
          <div class="col-9 my-1">
            <input type="number" name="weight" id="weight" value="<?php echo $weight ?>">
          </div>
          <div>
            Please Provide weight of the book in kg
          </div>
        </div>
        <div class="row" id="furnitureForm">
          <div class="col-3 my-1">
            Height (CM)
          </div>
          <div class="col-9 my-1">
            <input type="number" name="height" id="height" value="<?php echo $height ?>">
          </div>
          <div class="col-3 my-1">
            Width (CM)
          </div>
          <div class="col-9 my-1">
            <input type="number" name="width" id="width" value="<?php echo $width ?>">
          </div>
          <div class="col-3 my-1">
            Length (CM)
          </div>
          <div class="col-9 my-1">
            <input type="number" name="length" id="length" value="<?php echo $length ?>">
          </div>
          <div>
            Please Provide the dimensions in HxWxL format in CM
          </div>
        </div>
      </form>
    </div>
  </div>

  <script>
    const onSelectProductType = (selected = '') => {
      let productType
      if (selected) {
        // document.getElementById(selected).selected = true
        productType = selected
      } else
        productType = document.getElementById('productType').value
      for (const type of ['book', 'dvd', 'furniture']) {
        if (productType === type) {
          document.getElementById(`${type}Form`).classList.add('show')
        } else {
          document.getElementById(`${type}Form`).classList.remove('show')
        }
      }
    }

    const submitForm = () => {
      const form = document.getElementsByTagName('form')
      form[0].submit()
      console.log(form);
    }
  </script>

</body>

</html>