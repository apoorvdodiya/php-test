<?php
require_once "config.php";

try {
    $sqlDel = 'DELETE FROM product WHERE sku IN (';
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['deleteCheck']) && is_array($_POST['deleteCheck'])) {

        foreach ($_POST['deleteCheck'] as $deleteCheck) {
            $sqlDel = $sqlDel . '"' . $deleteCheck . '",';
        }
        $sqlDel =  substr($sqlDel, 0, -1) . ');';
        $stmt = mysqli_prepare($conn, $sqlDel);
        $res = mysqli_stmt_execute($stmt);
        header('Refresh: 0');
    }
} catch (Exception $ex) {
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Products</title>
    <link rel="stylesheet" href="common.css">
</head>

<body>
    <?php
    $sql = 'SELECT * FROM product;';
    $result = mysqli_query($conn, $sql);
    // echo mysqli_num_rows($result);
    // if (mysqli_num_rows($result) > 0) {
    // }

    ?>

    <div class="p-4">
        <div class="d-flex justify-content-between border-bottom pb-1">
            <h4 class="pb-0">
                Product List
            </h4>
            <div>
                <a class="btn btn-primary btn-sm me-2" href="add-product.php">ADD</a>
                <button id="delete-product-btn" class="btn btn-primary btn-sm" onclick="massDelete()">MASS DELETE</button>

            </div>
        </div>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="delete-form">
            <div class="mt-4 row">
                <?php
                while ($row = mysqli_fetch_array($result)) {
                    echo '
                    <div class="col-3 mb-4">
                        <div class="product-tile ">
                            <input type="checkbox" name="deleteCheck[]" class="delete-checkbox" value="' . $row['sku'] . '">
                            <span>' . $row['sku'] . '</span>
                            <span>' . $row['name'] . '</span>
                            <span>' . $row['price'] . ' $ </span>
                            <span>' . 
                            ($row['productType'] == 'dvd' ? 'Size: ' . $row['size'] . 'MB' :
                            ($row['productType'] == 'book' ? 'Wight: ' . $row['weight']  . 'KG' :
                            'Dimensions: ' . $row['height'] . 'x' . $row['width'] . 'x' . $row['length']))
                            . '</span>
                        </div>
                    </div>
                ';
                }
                ?>
            </div>
        </form>
    </div>

</body>

<script>
    const massDelete = () => {
        const checkList = document.getElementsByName('deleteCheck')
        console.log(checkList);
        // }
        // const submitForm = () => {
        const form = document.getElementsByTagName('form')
        form[0].submit()
        console.log(form);
    }
</script>

</html>